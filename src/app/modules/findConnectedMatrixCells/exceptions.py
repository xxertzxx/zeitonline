class ExceptionOnlyNumbersAllowed(Exception):
    def __str__(self):
        return 'nur Zahlen erlaubt'

class ExceptionNoText(Exception):
    def __str__(self):
        return 'leerer Text'

class ExceptionNumberOfTestCasesOutOfRange(Exception):
    def __str__(self):
        return 'Anzahl der Testfälle muss > 0 und < 6 sein'

class ExceptionMatrixDimensionOutOfRange(Exception):
    def __str__(self):
        return 'Matrix-Dimension N muss, 0 < N < 1009 sein'