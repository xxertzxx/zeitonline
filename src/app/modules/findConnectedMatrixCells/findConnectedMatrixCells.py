import os
import numpy as np
from scipy.ndimage.measurements import label

from pprint import pprint

from . import exceptions

class FindConnectedMatrixCells:
    def __init__(self, inputText: str):
        self.inputText = inputText

        self.numberOfTestCases = -1

        self.matrizen = []
        self.currentIndexCursor = 0
    
    def run(self)->str:
        try:
            resultArray = self.processInputText()
            return '\r\n'.join([str(s) for s in resultArray]).replace(' ', '')
        except Exception as exc:
            return str(exc)

    def processInputText(self):
        result = []
        self.inputText = self.inputText.replace(' ', '').replace('\r\n', '').replace('\r', '').replace('\n', '')

        if self.inputText == '':
            raise exceptions.ExceptionNoText()

        if self.inputText.isdigit() == False:
            raise exceptions.ExceptionOnlyNumbersAllowed()
        
        numberOfTestCases = int(self.inputText[self.currentIndexCursor])
        numberOfTestCases = int(numberOfTestCases)

        if numberOfTestCases < 1 or numberOfTestCases > 5:
            raise exceptions.ExceptionNumberOfTestCasesOutOfRange()

        self.currentIndexCursor += 1

        #print('numberOfTestCases:',numberOfTestCases)
        #print('inputstream:',self.inputText)
        #print('')

        for _ in range(numberOfTestCases):
            dim = int(self.inputText[self.currentIndexCursor])

            if dim < 1 or dim > 1008:
                raise exceptions.ExceptionMatrixDimensionOutOfRange()

            self.currentIndexCursor += 1

            mat = self.extractMatrixFromStream(dim)
            #print('stream:', mat)

            res = self.matrixStreamToArray(mat, dim)
            connectionCells = self.getConnectedCells(res)
            result.append(connectionCells)

        return result

    def getConnectedCells(self, mat: list)->int:
        struct = np.ones((3, 3), dtype=np.int)
        _, connectedCells = label(mat, struct)

        return connectedCells

    def extractMatrixFromStream(self, dim: int)->str:
        numOfCells = dim * dim
        
        mat = self.inputText[self.currentIndexCursor:numOfCells+self.currentIndexCursor]
        self.currentIndexCursor = numOfCells + self.currentIndexCursor

        return mat
    
    def matrixStreamToArray(self, mat: str, dim: int)->list:
        intMat = [int(s) for s in mat]
        
        mat = np.reshape(intMat, (dim, dim))
        return mat

if __name__ == "__main__":
    fcc = FindConnectedMatrixCells('440010101001001111410010000011010015100110010000000111110000080010010010000001001001010100010010000000001101101011011000000000')
    fcc.run()

"""
4
4
0 0 1 0
1 0 1 0
0 1 0 0
1 1 1 1
4
1 0 0 1
0 0 0 0
0 1 1 0
1 0 0 1
5
1 0 0 1 1
0 0 1 0 0
0 0 0 0 0
1 1 1 1 1
0 0 0 0 0
8
0 0 1 0 0 1 0 0
1 0 0 0 0 0 0 1
0 0 1 0 0 1 0 1
0 1 0 0 0 1 0 0
1 0 0 0 0 0 0 0
0 0 1 1 0 1 1 0
1 0 1 1 0 1 1 0
0 0 0 0 0 0 0 0
"""