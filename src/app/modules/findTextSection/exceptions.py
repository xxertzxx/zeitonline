class ExceptionNoSectionFound(Exception):
    def __str__(self):
        return 'KEIN ABSCHNITT GEFUNDEN'

class ExceptionToFewNumberOfSearchWords(Exception):
    def __str__(self):
        return 'Anzahl der zu suchenden Wörter muss > 0 sein'

class ExceptionTextPayloadTooLong(Exception):
    def __str__(self):
        return 'Zu viele Zeichen im Textabsatz'

class ExceptionNumberOfSearchWordsOutOfRange(Exception):
    def __str__(self):
        return 'Anzahl der Suchwörter k, 0 < k < Anzahl der Wörter im Absatz, sein'

class ExceptionSearchWordConditionEqual(Exception):
    def __str__(self):
        return 'Anzahl der Suchwörter entspricht nicht der definierten Anzahl'

class ExceptionNoText(Exception):
    def __str__(self):
        return 'leerer Text'

class ExceptionNoSearchWordFound(Exception):
    def __str__(self):
        return 'keine Suchwörter gefunden'