import sys
import os
import re
import json
import itertools

from pprint import pprint

from . import exceptions

class FindTextSection:
    MAX_NUMBER_OF_PAYLOAD_CHARS = 200000 if 'FLASK_TESTING' not in os.environ else 1000
    IGNORED_CHARACTERS = r'[^A-Za-z \n]'
    IGNORED_RESULTTEXT_CHARACTERS = r'[^A-Za-z ]'

    '''
        inputText {str} -- der komplette Inhalt des Textfeldes
    '''
    def __init__(self, inputText: str):
        # Eingegebener Textabschnitt
        self.inputText = inputText

        # Anzahl der nachfolgenden eingebenen Suchwörtern
        self.numberOfSearchWords = -1

        # Beinhaltet alle Wörter k, nach denen gesucht werden sollen
        self.searchWords = []

        # Beinhaltet die einzelnen Wörter im eingegebenen Textabschnitt
        self.inputTextAsArray = []

        # Die Hashtabelle beinhaltet die Indizes der Wörter eingegebenen Textabschnitt
        self.hashTable = {}

        # Dieses Array, beinhaltet Arrays mit Indizes, die alle zu suchenden Wörter enthält
        self.indexLists = []

        self.resultStartEndIndizes = {
            'startIndex': -1,
            'endIndex': -1,
        }
        self.numberOfPayloadChars = 0
    
    '''
        Startfunktion für dieses Modul
        @returns {str} -- das anzuzeigende Ergebnis
    '''
    def run(self)->str:
        try:
            self.processInputText()
            self.createHashTable()
            self.setIndexArrayWithWordMatches()
            self.findMinDistanceOfIndizesOfIndexList()
            return self.getResultTextSection()
        except Exception as exc:
            return str(exc)

    def processInputText(self):
        if self.inputText == '':
            raise exceptions.ExceptionNoText()

        lines = self.inputText.splitlines(keepends=True)

        for line in lines:
            if line == '\r' or line == '\n' or line == '\r\n':
                pass
            else:
                line = line.replace('\r\n', '').replace('\r', '').replace('\n', '')

            if self.numberOfSearchWords == -1 and line.isdigit():
                self.numberOfSearchWords = int(line)
            elif self.numberOfSearchWords > -1:
                self.searchWords.append(self.getHashKeyWord(line))
            else:
                # Da Zeilenumbrüche auch al Zeichen zählen, muss pro Zeile ein Zeichen addiert werden
                self.numberOfPayloadChars += len(line)+1
                words = [w for w in line.split(' ')]
                for word in words:
                    self.inputTextAsArray.append(word)

        if self.numberOfSearchWords < 1:
            raise exceptions.ExceptionToFewNumberOfSearchWords()
                
        if self.numberOfPayloadChars > self.MAX_NUMBER_OF_PAYLOAD_CHARS:
            raise exceptions.ExceptionTextPayloadTooLong()
        
        if len(self.searchWords) < 1:
            raise exceptions.ExceptionNoSearchWordFound()

        if len(self.searchWords) >= len(self.inputTextAsArray):
            raise exceptions.ExceptionNumberOfSearchWordsOutOfRange()

        if self.numberOfSearchWords != len(self.searchWords):
            raise exceptions.ExceptionSearchWordConditionEqual()

    def createHashTable(self):
        index = 0

        for word in self.inputTextAsArray:
            wordHashKey = self.getHashKeyWord(word)

            if wordHashKey in self.hashTable.keys():
                self.hashTable[wordHashKey].append(index)
            else:
                self.hashTable.update({
                    wordHashKey: [index],
                })
            index += 1
    
    def getHashKeyWord(self, word: str)->str:
        return re.sub(self.IGNORED_CHARACTERS, '', word.lower())

    def getResultText(self, text: str)->str:
        return re.sub(self.IGNORED_RESULTTEXT_CHARACTERS, '', text)

    def setIndexArrayWithWordMatches(self):
        for searchWord in self.searchWords:
            if searchWord in self.hashTable.keys():
                self.indexLists.append(self.hashTable[searchWord])
            else:
                raise exceptions.ExceptionNoSectionFound()
    
    def getResultTextSection(self)->str:
        startIndex = self.resultStartEndIndizes['startIndex']
        endIndex = self.resultStartEndIndizes['endIndex'
        ]+1
        return self.getResultText(' '.join(self.inputTextAsArray[startIndex:endIndex]))
    
    def findMinDistanceOfIndizesOfIndexList(self):
        currentMinDistance = -1
        minDistanceIndexList = []

        for indexList in list(itertools.product(*self.indexLists)):
            if len(indexList) < 1:
                continue

            distance = max(indexList) - min(indexList)
            
            if currentMinDistance == -1:
                currentMinDistance = distance
                minDistanceIndexList = indexList
                continue

            if distance == currentMinDistance:
                if sum(indexList) < sum(minDistanceIndexList):
                    currentMinDistance = distance
                    minDistanceIndexList = indexList
                    continue
                
            if distance < currentMinDistance:
                currentMinDistance = distance
                minDistanceIndexList = indexList

        if len(minDistanceIndexList) == 0:
            raise exceptions.ExceptionNoSectionFound()

        self.resultStartEndIndizes['startIndex'] = min(minDistanceIndexList)
        self.resultStartEndIndizes['endIndex'] = max(minDistanceIndexList)