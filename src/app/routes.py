from flask import render_template, request
from app import app

import app.modules.findTextSection.findTextSection as findTextSection
import app.modules.findConnectedMatrixCells.findConnectedMatrixCells as findConnectedMatrixCells

@app.route('/', methods=['POST', 'GET'])
def index():
    if request.method == 'GET':
        return initView()
    else:
        return resultView()

def initView():
     return render_template('index.html', initText='', result='')

def resultView():
    result = ''
    if 'exercise1' in request.form:
        modul1 = findTextSection.FindTextSection(request.form['inputText'])
        result = modul1.run()

    if 'exercise2' in request.form:
        modul2 = findConnectedMatrixCells.FindConnectedMatrixCells(request.form['inputText'])
        result = modul2.run()
    
    return render_template('index.html', initText=request.form['inputText'], result=result)