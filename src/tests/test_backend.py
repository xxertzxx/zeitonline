import os
import sys
import unittest
import numpy.testing as npt

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))

from app.modules.findTextSection import findTextSection
from app.modules.findConnectedMatrixCells import findConnectedMatrixCells

class TestCase(unittest.TestCase):
    def test_getConnectedCells(self):
        inputText = ''
        mat = [
            [0,0,1,0],
            [1,0,1,0],
            [0,1,0,1],
            [1,1,1,1]
        ]
        fcmc = findConnectedMatrixCells.FindConnectedMatrixCells(inputText)

        assert fcmc.getConnectedCells(mat) == 1

        mat = [
            [0,0,1,0],
            [1,0,1,0],
            [0,1,1],
            [1,1,1,1]
        ]

        with self.assertRaises(Exception):
            fcmc.getConnectedCells(mat)

        mat = [
            [0,0,1,0],
            [1,0,1,0],
            [0,1,1,'a'],
            [1,1,1,1]
        ]

        with self.assertRaises(Exception):
            fcmc.getConnectedCells(mat)
    
    def test_extractMatrixFromStream(self):
        inputText = '00101010010111111001001000100011'
        fcmc = findConnectedMatrixCells.FindConnectedMatrixCells(inputText)
        fcmc.currentIndexCursor = 0
        result = fcmc.extractMatrixFromStream(4)

        assert result == '0010101001011111'
    
    def test_matrixStreamToArray(self):
        fcmc = findConnectedMatrixCells.FindConnectedMatrixCells('')
        mat = '0010101001011111'
        dim = 4

        result = fcmc.matrixStreamToArray(mat, dim)
        expected_result = [[0,0,1,0],[1,0,1,0],[0,1,0,1],[1,1,1,1]]

        npt.assert_almost_equal(result, expected_result)

    def test_processInputText(self):
        inputText = '''test text
1
test
'''
        fts = findTextSection.FindTextSection(inputText)
        fts.processInputText()

        assert len(fts.inputTextAsArray) == 2
        assert fts.searchWords == ['test']
        assert fts.numberOfSearchWords == 1
    
    def test_hashTable(self):
        fts = findTextSection.FindTextSection('')
        fts.inputTextAsArray = ['test', 'text']

        fts.createHashTable()

        assert fts.hashTable == {
            'test': [0],
            'text': [1]
        }

        fts.inputTextAsArray = ['test', 'text', 'test']
        fts.hashTable = {}
        fts.createHashTable()

        assert fts.hashTable == {
            'test': [0, 2],
            'text': [1]
        }
    
    def test_hashKeyWord(self):
        fts = findTextSection.FindTextSection('')
        result = fts.getHashKeyWord('Wörter')

        assert result == 'wrter'
    
    def test_resultText(self):
        fts = findTextSection.FindTextSection('')
        result = fts.getResultText('Wörter')

        assert result == 'Wrter'
    
    def test_getResultTextSection(self):
        fts = findTextSection.FindTextSection('')
        fts.inputTextAsArray = ['ich', 'bin', 'ein', 'Beispieltext']
        fts.resultStartEndIndizes['startIndex'] = 0
        fts.resultStartEndIndizes['endIndex'] = 2

        result = fts.getResultTextSection()

        assert result == 'ich bin ein'
    
    def test_findMinDistanceOfIndizesOfIndexList(self):
        fts = findTextSection.FindTextSection('')
        fts.inputTextAsArray = ['Ein','toller','Beispieltext','ist','Blindtext.','Er','hat','ein','paar','Wörter.','Die','ist','ein','Beispieltext','der','ein','paar','Wörter','hat','und','auch','noch','ein','paar','mehr','um','die','Zeile','etwas','länger','zu','machen']
        fts.indexLists = [
            [0, 7, 12, 15, 22],
            [2, 13],
            [14],
            [8, 16, 23],
            [9, 17]
        ]

        fts.findMinDistanceOfIndizesOfIndexList()

        assert fts.resultStartEndIndizes == {
            'startIndex': 13,
            'endIndex': 17
        }

        fts.indexLists = []

        with self.assertRaises(Exception) as exc:
            fts.findMinDistanceOfIndizesOfIndexList()

        self.assertTrue('KEIN ABSCHNITT GEFUNDEN' in str(exc.exception))

if __name__ == "__main__":
    unittest.main(verbosity=2)