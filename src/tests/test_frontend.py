import sys
import os

import unittest
import urllib
import time

from flask import url_for
from selenium import webdriver

class TestCase(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome('/usr/bin/chromedriver')
        self.driver.get('http://127.0.0.1:5000/')
    
    def clearFields(self):
        self.driver.find_element_by_id("inputText").clear()
        self.driver.execute_script("document.querySelector('#solution pre').innerText = '';")

    def test_validInputText_Excercise1(self):
        test_inputtext_case1 = """Ein toller Beispieltext ist Blindtext. Er hat ein paar Wörter. Dies ist ein
Beispieltext, der ein paar Wörter hat und auch noch ein paar mehr, um die
Zeile etwas länger zu machen. Darüber hinaus ist er nur dafür da, um
genügend Testtext zusammenzubekommen. Dem Text selbst macht das nicht so
viel aus. Früher einmal mehr, als er noch nicht so selbstbewusst war. Heute
kennt er seine Rolle als Blindtext und fügt sich selbstbewusst ein. Er ist
ja irgendwie wichtig. Manchmal jedoch, ganz manchmal, weint er in der Nacht,
weil er niemals bis zum Ende gelesen wird. Doch das hat ja jetzt zum Glück
ein Ende.
5
ein
Beispieltext
der
paar
Wörter
"""
        expectedResult_case1 = 'Beispieltext der ein paar Wrter'

        test_inputtext_case2 = """Ein toller Beispieltext ist Blindtext. Er hat ein paar Wörter. Dies ist ein
Beispieltext, der ein paar Wörter hat und auch noch ein paar mehr, um die
Zeile etwas länger zu machen. Darüber hinaus ist er nur dafür da, um
genügend Testtext zusammenzubekommen. Dem Text selbst macht das nicht so
viel aus. Früher einmal mehr, als er noch nicht so selbstbewusst war. Heute
kennt er seine Rolle als Blindtext und fügt sich selbstbewusst ein. Er ist
ja irgendwie wichtig. Manchmal jedoch, ganz manchmal, weint er in der Nacht,
weil er niemals bis zum Ende gelesen wird. Doch das hat ja jetzt zum Glück
ein Ende.
3
hat
Wörter
ist
"""
        expectedResult_case2 = 'hat ein paar Wrter Dies ist'

        test_inputtext_case3 = """Ein toller Beispieltext ist Blindtext. Er hat ein paar Wörter. Dies ist ein
Beispieltext, der ein paar Wörter hat und auch noch ein paar mehr, um die
Zeile etwas länger zu machen. Darüber hinaus ist er nur dafür da, um
genügend Testtext zusammenzubekommen. Dem Text selbst macht das nicht so
viel aus. Früher einmal mehr, als er noch nicht so selbstbewusst war. Heute
kennt er seine Rolle als Blindtext und fügt sich selbstbewusst ein. Er ist
ja irgendwie wichtig. Manchmal jedoch, ganz manchmal, weint er in der Nacht,
weil er niemals bis zum Ende gelesen wird. Doch das hat ja jetzt zum Glück
ein Ende.
5
Unbekanntes
Wort
Beispieltext
ist
hat
"""
        expectedResult_case3 = 'KEIN ABSCHNITT GEFUNDEN'

        test_inputtext_case4 = ''
        expectedResult_case4 = 'leerer Text'

        test_inputtext_case5 = """Ein toller Beispieltext ist Blindtext. Er hat ein paar Wörter. Dies ist ein
Beispieltext, der ein paar Wörter hat und auch noch ein paar mehr, um die
Zeile etwas länger zu machen. Darüber hinaus ist er nur dafür da, um
genügend Testtext zusammenzubekommen. Dem Text selbst macht das nicht so
viel aus. Früher einmal mehr, als er noch nicht so selbstbewusst war. Heute
kennt er seine Rolle als Blindtext und fügt sich selbstbewusst ein. Er ist
ja irgendwie wichtig. Manchmal jedoch, ganz manchmal, weint er in der Nacht,
weil er niemals bis zum Ende gelesen wird. Doch das hat ja jetzt zum Glück
ein Ende.
Unbekanntes
Wort
Beispieltext
ist
hat
"""
        expectedResult_case5 = 'Anzahl der zu suchenden Wörter muss > 0 sein'

        test_inputtext_case6 = """Ein toller Beispieltext ist Blindtext. Er hat ein paar Wörter. Dies ist ein
Beispieltext, der ein paar Wörter hat und auch noch ein paar mehr, um die
Zeile etwas länger zu machen. Darüber hinaus ist er nur dafür da, um
genügend Testtext zusammenzubekommen. Dem Text selbst macht das nicht so
viel aus. Früher einmal mehr, als er noch nicht so selbstbewusst war. Heute
kennt er seine Rolle als Blindtext und fügt sich selbstbewusst ein. Er ist
ja irgendwie wichtig. Manchmal jedoch, ganz manchmal, weint er in der Nacht,
weil er niemals bis zum Ende gelesen wird. Doch das hat ja jetzt zum Glück
ein Ende.
5
"""
        expectedResult_case6 = 'keine Suchwörter gefunden'

        test_inputtext_case7 = """Ein toller Beispieltext ist Blindtext. Er hat ein paar Wörter. Dies ist ein
Beispieltext, der ein paar Wörter hat und auch noch ein paar mehr, um die
Zeile etwas länger zu machen. Darüber hinaus ist er nur dafür da, um
genügend Testtext zusammenzubekommen. Dem Text selbst macht das nicht so
viel aus. Früher einmal mehr, als er noch nicht so selbstbewusst war. Heute
kennt er seine Rolle als Blindtext und fügt sich selbstbewusst ein. Er ist
ja irgendwie wichtig. Manchmal jedoch, ganz manchmal, weint er in der Nacht,
weil er niemals bis zum Ende gelesen wird. Doch das hat ja jetzt zum Glück
ein Ende.
5
hat
"""
        expectedResult_case7 = 'Anzahl der Suchwörter entspricht nicht der definierten Anzahl'

        test_inputtext_case8 = """Ein toller Beispieltext ist Blindtext. Er hat ein paar Wörter. Dies ist ein
Beispieltext, der ein paar Wörter hat und auch noch ein paar mehr, um die
Zeile etwas länger zu machen. Darüber hinaus ist er nur dafür da, um
genügend Testtext zusammenzubekommen. Dem Text selbst macht das nicht so
viel aus. Früher einmal mehr, als er noch nicht so selbstbewusst war. Heute
kennt er seine Rolle als Blindtext und fügt sich selbstbewusst ein. Er ist
ja irgendwie wichtig. Manchmal jedoch, ganz manchmal, weint er in der Nacht,
weil er niemals bis zum Ende gelesen wird. Doch das hat ja jetzt zum Glück
ein Ende. Früher einmal mehr, als er noch nicht so selbstbewusst war. Heute
kennt er seine Rolle als Blindtext und fügt sich selbstbewusst ein. Er ist
ja irgendwie wichtig. Manchmal jedoch, ganz manchmal, weint er in der Nacht,
weil er niemals bis zum Ende gelesen wird. Doch das hat ja jetzt zum Glück
ein Ende. Doch das hat ja jetzt zum Glück
ein Ende. Doch das hat ja jetzt zum Glück
ein Ende. Blindtext
3
hat
Wörter
ist
"""
        expectedResult_case8 = 'Zu viele Zeichen im Textabsatz'

        test_inputtext_case9 = """Ein toller Beispieltext ist Blindtext.
5
hat
er
Text
Wörter
ist
"""
        expectedResult_case9 = 'Anzahl der Suchwörter k, 0 < k < Anzahl der Wörter im Absatz, sein'

        self.driver.find_element_by_id('inputText').send_keys(test_inputtext_case1)
        self.driver.find_element_by_id('btn1').click()
        time.sleep(1)

        resultmessage = self.driver.find_element_by_css_selector('#solution pre').text
        assert expectedResult_case1 in resultmessage

        self.clearFields()

        self.driver.find_element_by_id('inputText').send_keys(test_inputtext_case2)
        self.driver.find_element_by_id('btn1').click()
        time.sleep(1)

        resultmessage = self.driver.find_element_by_css_selector('#solution pre').text
        assert expectedResult_case2 in resultmessage

        self.clearFields()

        self.driver.find_element_by_id('inputText').send_keys(test_inputtext_case3)
        self.driver.find_element_by_id('btn1').click()
        time.sleep(1)

        resultmessage = self.driver.find_element_by_css_selector('#solution pre').text
        assert expectedResult_case3 in resultmessage

        self.clearFields()

        self.driver.find_element_by_id('inputText').send_keys(test_inputtext_case4)
        self.driver.find_element_by_id('btn1').click()
        time.sleep(1)

        resultmessage = self.driver.find_element_by_css_selector('#solution pre').text
        assert expectedResult_case4 in resultmessage

        self.clearFields()

        self.driver.find_element_by_id('inputText').send_keys(test_inputtext_case5)
        self.driver.find_element_by_id('btn1').click()
        time.sleep(1)

        resultmessage = self.driver.find_element_by_css_selector('#solution pre').text
        assert expectedResult_case5 in resultmessage

        self.clearFields()

        self.driver.find_element_by_id('inputText').send_keys(test_inputtext_case6)
        self.driver.find_element_by_id('btn1').click()
        time.sleep(1)

        resultmessage = self.driver.find_element_by_css_selector('#solution pre').text
        assert expectedResult_case6 in resultmessage

        self.clearFields()

        self.driver.find_element_by_id('inputText').send_keys(test_inputtext_case7)
        self.driver.find_element_by_id('btn1').click()
        time.sleep(1)

        resultmessage = self.driver.find_element_by_css_selector('#solution pre').text
        assert expectedResult_case7 in resultmessage

        self.clearFields()

        self.driver.find_element_by_id('inputText').send_keys(test_inputtext_case8)
        self.driver.find_element_by_id('btn1').click()
        time.sleep(1)

        resultmessage = self.driver.find_element_by_css_selector('#solution pre').text
        assert expectedResult_case8 in resultmessage

        self.clearFields()

        self.driver.find_element_by_id('inputText').send_keys(test_inputtext_case9)
        self.driver.find_element_by_id('btn1').click()
        time.sleep(1)

        resultmessage = self.driver.find_element_by_css_selector('#solution pre').text
        assert expectedResult_case9 in resultmessage

        self.clearFields()

    def test_validInputText_Excercise2(self):
        test_inputtext_case1 = """2
4
0 0 1 0
1 0 1 0
0 1 0 0
1 1 1 1
4
0 0 1 0
1 0 1 0
0 1 0 0
1 1 1 1
"""
        expectedResult_case1 = """1
1"""

        test_inputtext_case2 = """4
4
0 0 1 0
1 0 1 0
0 1 0 0
1 1 1 1
4
1 0 0 1
0 0 0 0
0 1 1 0
1 0 0 1
5
1 0 0 1 1
0 0 1 0 0
0 0 0 0 0
1 1 1 1 1
0 0 0 0 0
8
0 0 1 0 0 1 0 0
1 0 0 0 0 0 0 1
0 0 1 0 0 1 0 1
0 1 0 0 0 1 0 0
1 0 0 0 0 0 0 0
0 0 1 1 0 1 1 0
1 0 1 1 0 1 1 0
0 0 0 0 0 0 0 0
"""
        expectedResult_case2 = """1
3
3
9"""
        test_inputtext_case3 = """4
4
0 0 a 0
a 0 a 0
0 a 0 0
1 1 1 1
"""
        expectedResult_case3 = 'nur Zahlen erlaubt'

        test_inputtext_case4 = """6
4
0 0 1 0
1 0 1 0
0 1 0 0
1 1 1 1
"""
        expectedResult_case4 = 'Anzahl der Testfälle muss > 0 und < 6 sein'

        self.driver.find_element_by_id('inputText').send_keys(test_inputtext_case1)
        self.driver.find_element_by_id('btn2').click()
        time.sleep(1)

        resultmessage = self.driver.find_element_by_css_selector('#solution pre').text
        assert expectedResult_case1 in resultmessage

        self.clearFields()

        self.driver.find_element_by_id('inputText').send_keys(test_inputtext_case2)
        self.driver.find_element_by_id('btn2').click()
        time.sleep(1)

        resultmessage = self.driver.find_element_by_css_selector('#solution pre').text
        assert expectedResult_case2 in resultmessage

        self.clearFields()

        self.driver.find_element_by_id('inputText').send_keys(test_inputtext_case3)
        self.driver.find_element_by_id('btn2').click()
        time.sleep(1)

        resultmessage = self.driver.find_element_by_css_selector('#solution pre').text
        assert expectedResult_case3 in resultmessage

        self.clearFields()

        self.driver.find_element_by_id('inputText').send_keys(test_inputtext_case4)
        self.driver.find_element_by_id('btn2').click()
        time.sleep(1)

        resultmessage = self.driver.find_element_by_css_selector('#solution pre').text
        assert expectedResult_case4 in resultmessage

        self.clearFields()

    def tearDown(self):
        self.driver.close()
        self.driver.quit()

if __name__ == "__main__":
    unittest.main(verbosity=2)