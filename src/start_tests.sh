#!/bin/bash

export FLASK_TESTING=1

flask run &
python tests/test_backend.py
python tests/test_frontend.py

pkill -f flask

unset FLASK_TESTING