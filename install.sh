#!/bin/bash

sudo apt-get install python3-venv
sudo apt-get install chromium
sudo cp chromedriver /usr/bin

python3 -m venv .
source ./bin/activate
pip install -r ./src/requirements.txt
